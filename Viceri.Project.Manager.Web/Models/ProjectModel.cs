﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Viceri.Project.Manager.Web.Models
{
    public class ProjectModel
    {
        public int Id { get; set; }

        public int GitLabId { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public string CreatedAtFormat { get { return CreatedAt.ToString("dd/MM/yyyy"); } }

        public bool Import { get; set; }
    }
}