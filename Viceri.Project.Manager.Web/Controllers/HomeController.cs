﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Viceri.Project.Manager.Web.Models;

namespace Viceri.Project.Manager.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {



            return View();
        }

        public ActionResult ProjectListar()
        {
            try
            {
                var apiRequest = new ApiRequest(System.IO.Path.Combine(ConfigurationManager.AppSettings["ApiUrl"].ToString(), "api/project/listar"));
                var respostaApi = apiRequest.Get(null, null);
                var resultados = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProjectModel>>(respostaApi);


                return Json(new { erro = false, resultados }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { erro = true, erroMsg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ProjectImportar()
        {
            try
            {
                var apiRequest = new ApiRequest(System.IO.Path.Combine(ConfigurationManager.AppSettings["ApiUrl"].ToString(), "api/project/importar"));
                var respostaApi = apiRequest.Post(null, null);
                var resultados = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(respostaApi);


                return Json(new { erro = false, resultados }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { erro = true, erroMsg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult IssuesImportar()
        {
            try
            {
                var apiRequest = new ApiRequest(System.IO.Path.Combine(ConfigurationManager.AppSettings["ApiUrl"].ToString(), "api/issues/importar"));
                var respostaApi = apiRequest.Post(null, null);
                var resultados = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(respostaApi);


                return Json(new { erro = false, resultados }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { erro = true, erroMsg = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}