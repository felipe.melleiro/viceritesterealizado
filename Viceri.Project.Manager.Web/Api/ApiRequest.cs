﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

public class ApiRequest
{
    private readonly HttpWebRequest request;

    public ApiRequest(string url)
    {
        request = (HttpWebRequest)WebRequest.Create(url);

    }


    public string Post(NameValueCollection header, object data)
    {
        //Enviar
        request.ContentType = "application/json";
        request.Method = "POST";

        if (header != null && header.Count > 0)
            request.Headers.Add(header);

        if (data == null)
            data = new { Dummy = 1 };

        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        {
            var postData = Newtonsoft.Json.JsonConvert.SerializeObject(data);
            streamWriter.Write(postData);
            streamWriter.Flush();
        }

        //Receber
        var responseData = string.Empty;
        try
        {
            var response = (HttpWebResponse)request.GetResponse();
            using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                responseData = reader.ReadToEnd();
        }
        catch (WebException wex)
        {
            if (wex.Response != null)
            {
                var errorResponse = (HttpWebResponse)wex.Response;
                using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                    responseData = reader.ReadToEnd();
                throw new ArgumentException(responseData);
            }
        }

        return responseData;
    }

    public string Get(NameValueCollection header, object data)
    {
        //Enviar
        request.ContentType = "application/json";
        request.Method = "GET";

        if (header != null && header.Count > 0)
            request.Headers.Add(header);

        //Receber
        var response = (HttpWebResponse)request.GetResponse();
        var responseData = string.Empty;
        using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            responseData = reader.ReadToEnd();

        return responseData;
    }
}