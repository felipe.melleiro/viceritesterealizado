﻿var appViceri = new Vue({
    el: '#appViceri',
    data: {
        listProjects: [],
        loadDados: true
    },

    computed: {

    
    },

    methods: {

        importarProjetos() {

            appViceri.loadDados = true;

            $.ajaxJson({
                url: $("#importarProjetos").data("url"),
                success: function (json) {

                    if (json.erro) {
                        alert(json.erroMsg);
                    } else {
                        $.ajaxJson({
                            url: $("#appViceri").data("url"),
                            success: function (resultJson) {

                                if (resultJson.erro) {
                                    alert(resultJson.erroMsg);
                                } else {
                                    appViceri.listProjects = resultJson.resultados;
                                }

                                appViceri.loadDados = false;
                            }
                        });
                    }
                }
            });
        },

        importarIssues() {

            appViceri.loadDados = true;

            $.ajaxJson({
                url: $("#importarIssues").data("url"),
                success: function (json) {

                    if (json.erro) {
                        alert(json.erroMsg);
                    } 

                    appViceri.loadDados = false;
                }
            });
        }
    },

    mounted: function () {

        this.loadDados = true;
        $.ajaxJson({
            url: $("#appViceri").data("url"),
            success: function (resultJson) {

                if (resultJson.erro) {
                    alert(resultJson.erroMsg);
                } else {
                    appViceri.listProjects = resultJson.resultados;
                }

                appViceri.loadDados = false;
            }
        });


     
    }
});