﻿/************************************************************
* Exibe a caixa de mensagem de erro                         *
* By: Felipe Melleiro                                       *
* Date: 15/01/2016                                          *
*************************************************************/
jQuery.extend({
    ajaxPartialView: function (options) {

        var sDefaults = {
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            cache: false,
            traditional: true,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
              
                alert(errorThrown || "Erro interno. Tente novemente");
            }
        }

        var options = jQuery.extend(sDefaults, options);

        return $.ajax(options);
    },

    ajaxJson: function (options) {
        var sDefaults = {
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            cache: false,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {

                alert(errorThrown || "Erro interno. Tente novemente");
            }
        }

        var options = jQuery.extend(sDefaults, options);

        return $.ajax(options);
    }
});