﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.Api;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Application
{
    public class IssuesApplication : IIssuesApplication
    {
        private readonly IIssuesService _issuesService;

        public IssuesApplication(IIssuesService issuesService)
        {
            _issuesService = issuesService;
        }


        public void Importar()
        {
            var resultApi = new GitLabIssues().Listar();
            if (!resultApi.Erro)
            {
                foreach (var item in resultApi.Dados)
                {
                    this.Salvar(item.Id, item.Project_id, item.Title, item.Description, item.State, item.Created_at, item.web_url);
                }
            }

        }


        public void Salvar(int gitlabid, int projectid, string title, string description, string state, DateTime createdat, string weburl)
        {

            var model = _issuesService.Obter(gitlabid);
            if (model == null)
            {
                model = new Domain.Entities.Issues()
                {
                    GitLabId = gitlabid,
                    ProjectId = projectid,
                    Title = title,
                    Description = description,
                    State = state,
                    CreatedAt = createdat,
                    WebUrl = weburl
                };

                _issuesService.Adicionar(model);
            }
            else
            {
                model.GitLabId = gitlabid;
                model.ProjectId = projectid;
                model.Title = title;
                model.Description = description;
                model.State = state;
                model.CreatedAt = createdat;
                model.WebUrl = weburl;

                _issuesService.Editar(model);
            }

        }
    }
}
