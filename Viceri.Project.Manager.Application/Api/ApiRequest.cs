﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

public class ApiRequest
{
    private readonly HttpWebRequest request;

    public ApiRequest(string url)
    {
        request = (HttpWebRequest)WebRequest.Create(url);
    }


    public string Get(NameValueCollection header, object data)
    {
        //Enviar
        request.ContentType = "application/json";
        request.Method = "GET";

        if (header != null && header.Count > 0)
            request.Headers.Add(header);

        //Receber
        var response = (HttpWebResponse)request.GetResponse();
        var responseData = string.Empty;
        using (var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
            responseData = reader.ReadToEnd();

        return responseData;
    }


}
