﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.Api.DTO;

namespace Viceri.Project.Manager.Application.Api
{
    public class EstruturaApi<T>
    {
        public BaseEndPoint<T> ExecucaoPost(string apiurl, string apimodule, object model)
        {
            try
            {
                var apiRequest = new ApiRequest(System.IO.Path.Combine(apiurl, apimodule));
                var respostaApi = apiRequest.Get(null, model);
                var resultados = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(respostaApi);

                return new BaseEndPoint<T>()
                {
                    Erro = false,
                    Mensagem = "OK",
                    Excecao = null,
                    Dados = resultados
                };
            }
            catch (Exception ex)
            {
                var apiUrl = System.IO.Path.Combine(apiurl, apimodule);
                return new BaseEndPoint<T>()
                {
                    Erro = true,
                    Mensagem = "Falha ao acessar " + apiUrl + ": " + ex.Message,
                    Excecao = ex
                };
            }
        }
    }
}
