﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.Api.DTO;
using Viceri.Project.Manager.Domain.Dto.GitLab;

namespace Viceri.Project.Manager.Application.Api
{
    public class GitLabProject : BaseApi
    {
        public GitLabProject() : base(ConfigurationManager.AppSettings["GitLabUrl"].ToString()) { }

        public BaseEndPoint<List<ProjectDto>> Listar()
        {
            // EndPoint da API
            this.ApiModule = string.Format("api/v4/projects?private_token={0}", ConfigurationManager.AppSettings["GitLabToken"].ToString());

            // Executa a API via POST
            return new EstruturaApi<List<ProjectDto>>().ExecucaoPost(this.ApiUrl, this.ApiModule, null);
        }
    }
}
