﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application.Api
{
    public abstract class BaseApi
    {
        public string ApiUrl { get; set; }
        public string ApiModule { get; set; }

        protected BaseApi(string apiUrl)
        {
            this.ApiUrl = apiUrl;
        }

    }
}
