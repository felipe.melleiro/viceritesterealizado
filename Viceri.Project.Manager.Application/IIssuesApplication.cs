﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application
{
    public interface IIssuesApplication
    {
        void Importar();

        void Salvar(int gitlabid, int projectid, string title, string description, string state, DateTime createdat, string weburl);
    }
}
