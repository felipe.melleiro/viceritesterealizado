﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application
{
    public interface IProjectApplication
    {
        void Importar();

        List<ViewModels.ProjectViewModel> Listar();
    }
}
