﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application.ViewModels
{
    public class ProjectViewModel
    {
        public int Id { get; set; }

        public int GitLabId { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public bool Import { get; set; }
    }
}
