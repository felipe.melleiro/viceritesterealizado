﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Application.ViewModels
{
    public class IssuesViewModel
    {
        public string Object_kind { get; set; }
        public IssuesUserViewModel User { get; set; }
        public IssuesProjectViewModel Project { get; set; }
        public IssuesObjectAttributesViewModel Object_attributes { get; set; }
    }

    public class IssuesUserViewModel
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Avatar_url { get; set; }
    }

    public class IssuesProjectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class IssuesObjectAttributesViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string State { get; set; }
        public string Url { get; set; }
        public DateTime Created_at { get; set; }
    }
}
