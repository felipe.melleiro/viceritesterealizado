﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viceri.Project.Manager.Application.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool IsDeleted { get; set; }
    }
}
