﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Application.Api;
using Viceri.Project.Manager.Domain.Interfaces;
using Viceri.Project.Manager.Application.Extensions;

namespace Viceri.Project.Manager.Application
{
    public class ProjectApplication : IProjectApplication
    {
        private readonly IProjectService _projectService;

        public ProjectApplication(IProjectService projectService)
        {
            _projectService = projectService;
        }

        public List<ViewModels.ProjectViewModel> Listar()
        {
            var _lista = _projectService.Listar();
            return _lista.MapTo<List<ViewModels.ProjectViewModel>>();
        }

        public void Importar()
        {
            var resultApi = new GitLabProject().Listar();
            if (!resultApi.Erro)
            {
                var lista = resultApi.Dados.Where(w => w.Forked_from_project == null).ToList();
                foreach (var item in lista)
                {
                    this.Salvar(item.Id, item.Description, item.Name, item.Created_at);
                }
            }

        }


        private void Salvar(int gitlabid, string description, string name, DateTime createdat)
        {

            var model = _projectService.Obter(gitlabid);
            if (model == null)
            {
                model = new Domain.Entities.Project()
                {
                    GitLabId = gitlabid,
                    Description = description,
                    Name = name,
                    CreatedAt = createdat,
                    Import = true
                };

                _projectService.Adicionar(model);
            }
            else
            {
                model.Description = description;
                model.Name = name;
                model.CreatedAt = createdat;
                model.Import = true;

                _projectService.Editar(model);
            }

        }
    }
}
