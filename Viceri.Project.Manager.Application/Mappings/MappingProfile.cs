﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Viceri.Project.Manager.Application.Mappings
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.Entities.User, ViewModels.UserViewModel>();
            CreateMap<Domain.Entities.Project, ViewModels.ProjectViewModel>();
        }
    }
}
