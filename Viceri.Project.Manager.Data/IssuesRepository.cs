﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class IssuesRepository : IIssuesRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public IssuesRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public Issues Obter(int id)
        {
            return _context.Issues.FirstOrDefault(w => w.GitLabId == id);
        }

        public Issues Adicionar(Issues model)
        {
            _context.Set<Issues>().Add(model);
            _context.SaveChanges();

            return model;
        }

        public Issues Editar(Issues model)
        {
            _context.Entry(model).State = EntityState.Modified;
            _context.SaveChanges();

            return model;
        }
    }
}
