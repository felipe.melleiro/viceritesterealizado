﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using Viceri.Project.Manager.Data.EntityTypeConfigurations;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data
{
    public class ViceriProjectManagerContext : DbContext
    {

        public DbSet<User> Users { get; set; }

        public DbSet<Domain.Entities.Project> Projects { get; set; }

        public DbSet<Issues> Issues { get; set; }

        public ViceriProjectManagerContext() : base("name=DefaultConnection")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserTypeConfiguration());
            modelBuilder.Configurations.Add(new ProjectTypeConfiguration());
            modelBuilder.Configurations.Add(new IssuesTypeConfiguration());
        }
    }
}
