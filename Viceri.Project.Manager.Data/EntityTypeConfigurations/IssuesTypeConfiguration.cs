﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class IssuesTypeConfiguration : EntityTypeConfiguration<Issues>
    {
        public IssuesTypeConfiguration()
        {
            this.ToTable("Issues");
        }
    }
}
