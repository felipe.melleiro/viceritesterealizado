﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Text;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class UserTypeConfiguration : EntityTypeConfiguration<User>
    {
        public UserTypeConfiguration()
        {
            this.ToTable("User");
        }
    }
}
