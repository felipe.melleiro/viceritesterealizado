﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Data.EntityTypeConfigurations
{
    public class ProjectTypeConfiguration : EntityTypeConfiguration<Domain.Entities.Project>
    {
        public ProjectTypeConfiguration()
        {
            this.ToTable("Project");
        }
    }
}
