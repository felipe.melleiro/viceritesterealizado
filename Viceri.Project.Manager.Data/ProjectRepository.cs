﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Data
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ViceriProjectManagerContext _context;

        public ProjectRepository(ViceriProjectManagerContext context)
        {
            _context = context;
        }

        public Domain.Entities.Project Obter(int id)
        {
            return _context.Projects.FirstOrDefault(w => w.GitLabId == id);
        }

        public List<Domain.Entities.Project> Listar()
        {
            return _context.Projects.ToList();
        }

        public Domain.Entities.Project Adicionar(Domain.Entities.Project model)
        {
            _context.Set<Domain.Entities.Project>().Add(model);
            _context.SaveChanges();

            return model;
        }

        public Domain.Entities.Project Editar(Domain.Entities.Project model)
        {
            _context.Entry(model).State = EntityState.Modified;
            _context.SaveChanges();

            return model;
        }
    }
}
