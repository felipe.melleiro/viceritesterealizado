namespace Viceri.Project.Manager.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class viceri : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Issues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GitLabId = c.Int(nullable: false),
                        ProjectId = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        State = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        WebUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Project",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GitLabId = c.Int(nullable: false),
                        Description = c.String(),
                        Name = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        Import = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.User");
            DropTable("dbo.Project");
            DropTable("dbo.Issues");
        }
    }
}
