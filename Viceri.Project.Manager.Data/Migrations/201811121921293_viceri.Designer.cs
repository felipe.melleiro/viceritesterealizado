// <auto-generated />
namespace Viceri.Project.Manager.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class viceri : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(viceri));
        
        string IMigrationMetadata.Id
        {
            get { return "201811121921293_viceri"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
