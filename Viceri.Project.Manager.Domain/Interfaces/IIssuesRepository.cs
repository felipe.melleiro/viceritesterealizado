﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IIssuesRepository
    {
        Issues Obter(int id);

        Issues Adicionar(Issues model);

        Issues Editar(Issues model);
    }
}
