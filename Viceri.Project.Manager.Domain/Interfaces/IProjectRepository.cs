﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Interfaces
{
    public interface IProjectRepository
    {
        Domain.Entities.Project Obter(int id);

        List<Domain.Entities.Project> Listar();

        Domain.Entities.Project Adicionar(Domain.Entities.Project model);

        Domain.Entities.Project Editar(Domain.Entities.Project model);
    }
}
