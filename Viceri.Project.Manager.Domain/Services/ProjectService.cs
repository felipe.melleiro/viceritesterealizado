﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public Domain.Entities.Project Obter(int id)
        {
            return _projectRepository.Obter(id);
        }

        public List<Domain.Entities.Project> Listar()
        {
            return _projectRepository.Listar();
        }

        public Domain.Entities.Project Adicionar(Domain.Entities.Project model)
        {
            return _projectRepository.Adicionar(model);
        }

        public Domain.Entities.Project Editar(Domain.Entities.Project model)
        {
            return _projectRepository.Editar(model);
        }
    }
}
