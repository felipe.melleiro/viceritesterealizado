﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viceri.Project.Manager.Domain.Entities;
using Viceri.Project.Manager.Domain.Interfaces;

namespace Viceri.Project.Manager.Domain.Services
{
    public class IssuesService : IIssuesService
    {
        private readonly IIssuesRepository _issuesRepository;

        public IssuesService(IIssuesRepository issuesRepository)
        {
            _issuesRepository = issuesRepository;
        }

        public Issues Obter(int id)
        {
            return _issuesRepository.Obter(id);
        }

        public Issues Adicionar(Issues model)
        {
            return _issuesRepository.Adicionar(model);
        }

        public Issues Editar(Issues model)
        {
            return _issuesRepository.Editar(model);
        }
    }
}
