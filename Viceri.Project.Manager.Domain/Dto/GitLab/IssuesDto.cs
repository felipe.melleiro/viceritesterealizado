﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Dto.GitLab
{
    public class IssuesDto
    {
        public int Id { get; set; }


        public int Project_id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string State { get; set; }

        public DateTime Created_at { get; set; }

        public string web_url { get; set; }
    }
}
