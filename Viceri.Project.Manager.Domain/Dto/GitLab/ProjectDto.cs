﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viceri.Project.Manager.Domain.Dto.GitLab
{
    public class ProjectDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public DateTime Created_at { get; set; }

        public int Forks_count { get; set; }

        public Object Forked_from_project { get; set; }
    }
}
