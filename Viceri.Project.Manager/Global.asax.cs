﻿using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Viceri.Project.Manager
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ConfigureSimpleInjector();
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Application.Mappings.MappingProfile>();
            });
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        private static void ConfigureSimpleInjector()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            IoC.IoCConfiguration.Configure(container);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.Verify();
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
