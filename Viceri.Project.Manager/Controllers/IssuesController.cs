﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.ViewModels;

namespace Viceri.Project.Manager.Controllers
{
    [RoutePrefix("api/issues")]
    public class IssuesController : ApiController
    {
        private readonly IIssuesApplication _issuesApplication;

        public IssuesController(IIssuesApplication issuesApplication)
        {
            _issuesApplication = issuesApplication;

        }

        [AcceptVerbs("POST")]
        [Route("importar")]
        public Task<HttpResponseMessage> Importar()
        {
            HttpResponseMessage response = null;

            try
            {
                _issuesApplication.Importar();

                response = Request.CreateResponse(HttpStatusCode.OK, "OK");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.GetBaseException().Message);
            }

            return Task.FromResult(response);
        }

        [AcceptVerbs("POST")]
        [Route("atualizar")]
        public Task<HttpResponseMessage> Atualizar(IssuesViewModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                _issuesApplication.Salvar(model.Object_attributes.Id, model.Project.Id, model.Object_attributes.Title, model.Object_attributes.Description, model.Object_attributes.State, model.Object_attributes.Created_at, model.Object_attributes.Url);

                response = Request.CreateResponse(HttpStatusCode.OK, "OK");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.GetBaseException().Message);
            }

            return Task.FromResult(response);
        }
    }
}
