﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Viceri.Project.Manager.Application;
using Viceri.Project.Manager.Application.Api;

namespace Viceri.Project.Manager.Controllers
{
    [RoutePrefix("api/project")]
    public class ProjectController : ApiController
    {
        private readonly IProjectApplication _projectApplication;

        public ProjectController(IProjectApplication projectApplication)
        {
            _projectApplication = projectApplication;

        }

        [AcceptVerbs("POST")]
        [Route("importar")]
        public Task<HttpResponseMessage> Importar()
        {
            HttpResponseMessage response = null;

            try
            {
                _projectApplication.Importar();

                response = Request.CreateResponse(HttpStatusCode.OK, "OK");
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.GetBaseException().Message);
            }

            return Task.FromResult(response);
        }

        [AcceptVerbs("GET")]
        [Route("listar")]
        public Task<HttpResponseMessage> Listar()
        {
            HttpResponseMessage response = null;

            try
            {
                var retorno = _projectApplication.Listar();

                response = Request.CreateResponse(HttpStatusCode.OK, retorno);
            }
            catch (Exception ex)
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.GetBaseException().Message);
            }

            return Task.FromResult(response);
        }
    }
}
