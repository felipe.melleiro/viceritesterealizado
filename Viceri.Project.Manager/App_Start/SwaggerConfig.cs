using System.Web.Http;
using WebActivatorEx;
using Viceri.Project.Manager;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Viceri.Project.Manager
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {

                        c.SingleApiVersion("v1", "Viceri.Project.Manager");

                    })
                .EnableSwaggerUi();
        }
    }
}
